﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateToMouse : MonoBehaviour
{
    // This script will rotate whatever it's attached to, to face the cursor,

    public Camera cam;
    private float maxRayLength = 100;

    private Ray mouseRay;
    private RaycastHit rayHit;

    private Vector3 pos;
    private Vector3 desiredDirection;
    private Quaternion desiredRotation;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        var mousePos = Input.mousePosition;
        mouseRay = cam.ScreenPointToRay(mousePos);
        if(Physics.Raycast(mouseRay.origin, mouseRay.direction, out rayHit, maxRayLength))
        {
            RotateObjectToMouseDirection(gameObject, rayHit.point);
        }
        else
        {
            var pos = mouseRay.GetPoint(maxRayLength);
            RotateObjectToMouseDirection(gameObject, rayHit.point);
        }
    }

    void RotateObjectToMouseDirection(GameObject obj, Vector3 destination)
    {
        destination.y = 0;
        desiredDirection = destination - obj.transform.position;
        desiredRotation = Quaternion.LookRotation(desiredDirection);
        desiredRotation.x = 0;
        desiredRotation.z = 0;
        obj.transform.localRotation = Quaternion.Lerp(transform.rotation, desiredRotation, 1);
    }
}
