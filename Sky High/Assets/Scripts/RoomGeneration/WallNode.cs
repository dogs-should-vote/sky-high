﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallNode : MonoBehaviour
{
    public WallType wallType;
    public WallNode wallLink;

    //initilize the wall node if there is not already a walltype declared
    public void Initilize(WallType wallTypeInput)
    {
        wallType = wallTypeInput;
    }

    //links this node and another wall node together
    public void WallLink(WallNode linkNode)
    {
        wallLink = linkNode;

        //ensure we do not go into an endless loop function by adding an if statment
        if (linkNode.wallLink == null)
            linkNode.WallLink(this);
    }

    public void WallLink(GameObject linkNodeObject)
    {
        wallLink = linkNodeObject.GetComponent<WallNode>();

        //ensure we do not go into an endless loop function by adding an if statment
        if(linkNodeObject.GetComponent<WallNode>().wallLink == null)
            linkNodeObject.GetComponent<WallNode>().WallLink(this);
    }

}
